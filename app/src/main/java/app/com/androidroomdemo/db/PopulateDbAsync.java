package app.com.androidroomdemo.db;

import android.os.AsyncTask;

import app.com.androidroomdemo.dao.WordDao;
import app.com.androidroomdemo.pojo.Word;

public class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

    private final WordDao mDao;

    PopulateDbAsync(WordRoomDatabase db) {
        mDao = db.wordDao();
    }

    @Override
    protected Void doInBackground(final Void... params) {
        mDao.deleteAll();
        Word word = new Word("Hello", "asd");
        mDao.insert(word);
        word = new Word("World", "asdasd");
        mDao.insert(word);
        return null;
    }
}