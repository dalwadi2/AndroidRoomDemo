package app.com.androidroomdemo.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import app.com.androidroomdemo.pojo.Word;

@Dao
public interface WordDao {

    @Insert
    void insert(Word word);

    @Query("delete from word_table")
    void deleteAll();

    @Query("SELECT * from word_table order by word asc")
    LiveData<List<Word>> getAllWords();
}
